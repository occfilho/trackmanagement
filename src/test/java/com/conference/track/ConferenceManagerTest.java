package com.conference.track;

import com.conference.track.model.*;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * By Oliveira Campos
 */
public class ConferenceManagerTest {

    private Conference getConference() {

        Calendar calendar = Calendar.getInstance(Locale.getDefault());

        calendar.set(2018, Calendar.OCTOBER, 10, 9, 0, 0);
        Date mornig = calendar.getTime();

        calendar.set(2018, Calendar.OCTOBER, 10, 12, 0, 0);
        Date beginLunch = calendar.getTime();

        calendar.set(2018, Calendar.OCTOBER, 10, 13, 0, 0);
        Date endLunch = calendar.getTime();

        calendar.set(2018, Calendar.OCTOBER, 10, 16, 0, 0);
        Date networkingEvent = calendar.getTime();


        return new Conference(mornig, new Lunch("Lunch", beginLunch, endLunch),
                new Networking("Networking", networkingEvent, 60));
    }

    @Test
    public void loadFile() {

        var conference = getConference();
        var conferenceManager = new ConferenceManager(conference);
        conferenceManager.loadFile("./input.txt");
        conferenceManager.show();

    }

    @Test
    public void loadFileAndCheckIfAllLinesWasCharged() throws IOException {

        var conference = getConference();
        var conferenceManager = new ConferenceManager(conference);
        conferenceManager.loadFile("./input.txt");
        List<Track> tracks = conferenceManager.getMeetings();

        var tracksSize = tracks.stream().flatMap(a -> a.getEvents().stream()).filter(b -> b instanceof Talk).collect(Collectors.toList()).size();
        var fileSize = Files.lines(new File("./input.txt").toPath()).count();

        assertEquals(tracksSize, fileSize);

    }

    @Test
    public void loadEmptyFile() {

        var conference = getConference();
        var conferenceManager = new ConferenceManager(conference);
        conferenceManager.loadFile("./inputEmpty.txt");
        assertEquals(conferenceManager.getMeetings().size(), 0);

    }

    @Test
    public void loadADamageFile() {

        var conference = getConference();
        var conferenceManager = new ConferenceManager(conference);
        conferenceManager.loadFile("./inputDamaged.txt");
        assertEquals(conferenceManager.getMeetings().size(), 0);

    }

    @Test
    public void loadAFileWithSomeDamagedLines() {

        var conference = getConference();
        var conferenceManager = new ConferenceManager(conference);
        conferenceManager.loadFile("./inputSomeDamagedLines.txt");

    }

    @Test
    public void loadANonexistentFile() {
        var conference = getConference();
        var conferenceManager = new ConferenceManager(conference);
        conferenceManager.loadFile("./nonexistent.txt");
    }


}
