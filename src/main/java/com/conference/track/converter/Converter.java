package com.conference.track.converter;

import com.conference.track.model.Talk;

import static com.conference.track.message.Messages.LIGHTNING;
import static com.conference.track.message.Messages.MIN;

public class Converter {

    public static Talk convert(String line) {
        var index = returnIndex(line);
        return new Talk(getTalkTitle(index, line), getTalkDuration(index, line));
    }

    private static String getTalkTitle(Integer index, String line) {
        return line.substring(0, index - 2);
    }

    private static Integer getTalkDuration(Integer index, String line) {
        return line.toLowerCase().contains(LIGHTNING)
                ? 5
                : Integer.valueOf(line.substring(index - 2, index));
    }

    private static Integer returnIndex(String line) {
        return line.toLowerCase().contains(MIN)
                ? line.toLowerCase().lastIndexOf(MIN)
                : line.toLowerCase().lastIndexOf(LIGHTNING);
    }

}
