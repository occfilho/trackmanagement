package com.conference.track.model;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class Track {

    private String title;
    private Date begin;
    private Lunch lunch;
    private Networking networkingEvent;
    private List<Event> events = new ArrayList<>();

    public Track(String title,
                 Date begin,
                 Lunch lunch,
                 Networking networkingEvent) {

        this.title = title;
        this.begin = begin;
        this.lunch = lunch;
        this.networkingEvent = networkingEvent;
    }

    public String getTitle() {
        return title;
    }

    private Integer getPeriodInMinutes(Date begin, Date end) {
        return Long.valueOf(end.getTime() / 1000 / 60).intValue()
                - Long.valueOf(begin.getTime() / 1000 / 60).intValue();
    }

    private Integer getTotalMinutes(List<Event> events) {
        return events.stream().map(Event::getDurationInMinute).reduce((a, b) -> a + b).orElse(0);
    }

    private Integer getTotalMilliseconds(List<Event> events) {
        return events.stream().map(Event::getDurationInMinute).reduce((a, b) -> a + b).orElse(0) * 60 * 1000;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void loadSession(List<Event> events) {

        var morning = new ArrayList<Event>();

        var afternoon = new ArrayList<Event>();

        events.stream().sorted(Comparator.comparing(Event::getDurationInMinute).reversed()).forEach(event -> {

            if (event.getDurationInMinute() <= getPeriodInMinutes(begin, lunch.getBegin()) - getTotalMinutes(morning)) {

                event.addTimeSlice(begin.getTime() + getTotalMilliseconds(morning));
                morning.add(event);

            } else if (event.getDurationInMinute() <= (getPeriodInMinutes(lunch.getBegin(), networkingEvent.getBegin()) +
                    networkingEvent.getToleranceInMinute()) - getTotalMinutes(afternoon)) {

                if (!afternoon.contains(lunch))
                    afternoon.add(lunch);

                event.addTimeSlice(lunch.getBegin().getTime() + getTotalMilliseconds(afternoon));
                afternoon.add(event);

            }
        });

        this.events.addAll(morning);
        this.events.addAll(afternoon);

        var netWorking = new Networking(networkingEvent.getTitle(), new Date(begin.getTime()
                + getTotalMilliseconds(this.events)), networkingEvent.getToleranceInMinute());
        this.events.add(netWorking);

        events.removeAll(this.events);

    }

}
