package com.conference.track.model;

import java.util.Date;

public class Lunch extends Event {

    private Date end;

    public Lunch(String title, Date begin, Date end) {
        super(title);
        this.begin = begin;
        this.end = end;
    }

    public Date getBegin() {
        return begin;
    }

    public Date getEnd() {
        return end;
    }

    @Override
    public Integer getDurationInMinute() {
        return Long.valueOf(getEnd().getTime() / 1000 / 60).intValue() - Long.valueOf(begin.getTime() / 1000 / 60).intValue();
    }

    @Override
    Integer getToleranceInMinute() {
        return 0;
    }

}
