package com.conference.track.model;

import java.util.Date;

public class Conference {

    private Date begin;
    private Lunch lunch;
    private Networking networkingEvent;

    public Conference(Date begin, Lunch lunch, Networking networkingEvent) {
        this.begin = begin;
        this.lunch = lunch;
        this.networkingEvent = networkingEvent;
    }

    public Date getBegin() {
        return begin;
    }

    public Lunch getLunch() {
        return lunch;
    }

    public Networking getNetworkingEvent() {
        return networkingEvent;
    }
}
