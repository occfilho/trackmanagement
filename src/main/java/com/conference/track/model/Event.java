package com.conference.track.model;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import static com.conference.track.message.Messages.MIN;

public abstract class Event {

    private String title;
    Date begin;

    Event(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public abstract Integer getDurationInMinute();

    public String getDuration() {
        return getDurationInMinute() + MIN;
    }

    abstract Integer getToleranceInMinute();

    public void addTimeSlice(Long millis) {
        begin = new Date(millis);
    }

    public String getStartingTime() {
        String time = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault()).format(begin);
        return time.length() < 8 ? "0" + time : time;
    }


}
