package com.conference.track.model;

public class Talk extends Event {

    private Integer durationMinute;

    public Talk(String title, Integer durationMinute) {
        super(title);
        this.durationMinute = durationMinute;
    }

    public Integer getDurationInMinute() {
        return this.durationMinute;
    }

    @Override
    Integer getToleranceInMinute() {
        return 0;
    }
}
