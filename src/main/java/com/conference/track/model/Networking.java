package com.conference.track.model;

import java.util.Date;

public class Networking extends Event {

    private Integer toleranceInMinute;

    public Networking(String title, Date begin, Integer toleranceInMinute) {
        super(title);
        this.begin = begin;
        this.toleranceInMinute = toleranceInMinute;
    }

    public Date getBegin() {
        return begin;
    }

    @Override
    public Integer getDurationInMinute() {
        return Long.valueOf(begin.getTime() / 1000 / 60).intValue()
                - 120;
    }

    @Override
    public String getDuration() {
        return "";
    }

    @Override
    Integer getToleranceInMinute() {
        return toleranceInMinute;
    }

}
