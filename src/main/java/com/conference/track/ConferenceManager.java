package com.conference.track;

import com.conference.track.converter.Converter;
import com.conference.track.validator.ErrorWriter;
import com.conference.track.validator.Validator;
import com.conference.track.model.Conference;
import com.conference.track.model.Event;
import com.conference.track.model.Talk;
import com.conference.track.model.Track;
import com.conference.track.printer.TerminalPrinter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static com.conference.track.message.Messages.*;
import static java.nio.file.Files.lines;

/**
 * By Oliveira Campos
 */
public class ConferenceManager implements ErrorWriter {

    private Conference conference;
    private List<Track> meetings = new ArrayList<>();

    public ConferenceManager(Conference conference) {
        this.conference = conference;
    }

    public List<Track> getMeetings() {
        return List.copyOf(meetings);
    }

    public void loadFile(String filePath) {

        try {
            var file = new File(filePath);
            if (file.exists()) {
                loadLines(lines(file.toPath()));
            } else {
                System.out.println(FILE_NONEEXISTENT);
            }
        } catch (IOException e) {
            System.out.println(INVALID_FILE);
        }

    }

    private void loadLines(Stream<String> lines) {
        createMeeting(convertToTalk(lines));
    }

    private List<Talk> convertToTalk(Stream<String> lines) {
        return lines.filter(new Validator(this)::validate)
                .map(Converter::convert).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    private void createMeeting(List<Talk> talks) {
        if (!talks.isEmpty()) {
            createTracks(conference, talks.stream().collect(ArrayList::new, ArrayList::add, ArrayList::addAll));
        } else {
            System.out.println(EMPTY_FILE_OR_INVALID_FORMAT);
        }
    }

    private void createTracks(Conference conference, List<Event> events) {

        int count = 0;

        while (!events.isEmpty()) {

            var track = new Track(String.format(TRACK, ++count),
                    conference.getBegin(),
                    conference.getLunch(),
                    conference.getNetworkingEvent());

            track.loadSession(events);

            meetings.add(track);

        }

    }

    public void show() {

        TerminalPrinter.print(meetings);

    }


    @Override
    public void write(String message) {
        System.out.println(message);
    }

}
