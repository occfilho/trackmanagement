package com.conference.track.printer;

import com.conference.track.model.Track;

import java.util.List;

public class TerminalPrinter {

    public static void print(List<Track> meetings) {
        meetings.forEach(TerminalPrinter::output);
    }

    private static void output(Track track) {

        System.out.println();
        System.out.println(track.getTitle());

        track.getEvents().forEach(event -> {
            System.out.print(event.getStartingTime() + " ");
            System.out.print(event.getTitle() + " ");
            System.out.println(event.getDuration());

        });

    }

}
