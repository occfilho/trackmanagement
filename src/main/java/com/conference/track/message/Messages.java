package com.conference.track.message;

public class Messages {

    //Used inside ConferenceManager class
    public static String FILE_NONEEXISTENT = "\nFile nonexistent";
    public static String EMPTY_FILE_OR_INVALID_FORMAT = "\n Empty file or invalid format";
    public static String INVALID_FILE = "\nInvalid file";
    public static String TRACK = "Track %s:";

    //Used inside Converter, Event and Validator class
    public static String LIGHTNING = "lightning";
    public static String MIN = "min";

    //Used inside Validator class
    public static String INVALID_FORMAT_AT_LINE = "Invalid format at line ";
    public static String ERRORS_FOUND = "\nErrors found\n" + INVALID_FORMAT_AT_LINE;

}
