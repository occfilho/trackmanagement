package com.conference.track.validator;

import static com.conference.track.message.Messages.*;

public class Validator {

    private int index = 0;
    private int indexError = 0;
    private ErrorWriter errorWriter;

    public Validator(ErrorWriter errorWriter) {
        this.errorWriter = errorWriter;
    }

    public Boolean validate(String line) {
        ++index;
        var ok = validateMin(line) || validateLightning(line);
        if (!ok) {
            ++indexError;
            String message = indexError != 1
                    ? INVALID_FORMAT_AT_LINE + index : ERRORS_FOUND + index;
            errorWriter.write(message);
        }
        return ok;
    }

    public Boolean validateMin(String line) {
        int index = line.toLowerCase().lastIndexOf(MIN);
        return ifContain(MIN, line)
                && ifContainHourOnly(index, line)
                && ifContainTitle(index, line);
    }

    public Boolean validateLightning(String line) {
        int index = line.toLowerCase().indexOf(LIGHTNING);
        return ifContain(LIGHTNING, line)
                && ifContainTitle(index, line);
    }

    private Boolean ifContain(String description, String line) {
        return line.toLowerCase().contains(description);
    }

    private Boolean ifContainHourOnly(int index, String line) {
        return line.substring(index - 2, index).matches("[0-9]+");
    }

    private Boolean ifContainTitle(int index, String line) {
        return !line.substring(0, index - 2).trim().isEmpty();
    }


}
