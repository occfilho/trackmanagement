package com.conference.track.validator;

public interface ErrorWriter {

    void write(String message);

}
